package com.cdac.controller;

import java.io.FileNotFoundException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.cdac.exception.CustomException;
import com.cdac.model.RegistrationFormModel;

@Controller
public class RegistrationController {

	@GetMapping(value = "/register", produces = "text/html")
	public String showRegistrationPage(Model model) {

		model.addAttribute("command", new RegistrationFormModel());

		return "registrationPage";
	}

	/*
	 * What ever the exception just throw it from method as we have handler all
	 * exception centrally
	 */
	@PostMapping(value = "/register", produces = "text/html")
	public String register(@ModelAttribute("command") RegistrationFormModel registrationFormModel)
			throws CustomException, FileNotFoundException {
		if (registrationFormModel.getName().contains("runtime")) {
			throw new RuntimeException("Runtime Exception Occured");
		} else if (registrationFormModel.getName().contains("custom")) {
			System.out.println("Here");
			throw new CustomException("Custom Exception Occured");
		} else if (registrationFormModel.getName().contains("file")) {
			throw new FileNotFoundException("File Not Found Exception Occured");
		}

		return "success";
	}

}
