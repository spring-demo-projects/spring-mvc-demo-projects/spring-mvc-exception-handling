<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Form</title>

<!-- To avoid favicon request -->
<link rel="icon" href="data:,">

</head>
<body>
	<div>
		<springForm:form action="register">

			<div>
				<label for="name">Name : </label>
				<springForm:input path="name" />
			</div>

			<div>
				<input type="submit" value="Submit" />
			</div>
		</springForm:form>
	</div>
</body>
</html>